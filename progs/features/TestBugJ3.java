class Test {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;
        x += y; // Bug: x is not modified
        System.out.println("The value of x: " + x); // Expected output: The value of x: 5
    }
}