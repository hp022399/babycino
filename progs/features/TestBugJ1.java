class Test {
    public static void main(String[] args) {
        int x = 5;
        boolean y = true; // boolean expression
        x += y; // Bug: boolean expression used in += statement
        System.out.println("The value of x: " + x); // Expected output: The value of x: 5
    }
}