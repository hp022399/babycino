class Test {
    public static void main(String[] args) {
        int x;
        boolean y;
        
        x = 10; // Assign an integer value to x
        y = x < 5; // Try to assign a boolean expression to y
        
        if (y) {
            System.out.println("Y is true");
        } else {
            System.out.println("Y is false");
        }
    }
}

//second bug program
class Test {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;

        // Bug: Incorrect assignment (y assigned to x instead of x + y)
        x = y;

        // Print different messages based on the value of x
        if (x == 15) {
            System.out.println("Correct compiler: x is assigned the sum of x and y (15).");
        } else if (x == 10) {
            System.out.println("Incorrect compiler: x is incorrectly assigned y (10).");
        } else {
            System.out.println("Unexpected value of x: " + x);
        }
    }
}